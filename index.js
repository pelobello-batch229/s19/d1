// console.log("hello");

// [SECTION] - if, else if, else statement

let	numG= -1;

//  if statement
// Executes a statement if a specified condition is true

// This will not excute since the condition is not met or it is "false"
if (numG < -5) {
	console.log("Hello")
}
// This will execute since the condition was met or it is "true"
if (numG < 1) {
	console.log("Hello")
}


// else if statement
/*
1. Execute a statement if previous condition are false and if the specified condition is true.
2. The "else if" clause is optional and can be added to capture additional condition to change the flow of a program.
*/
let numH = 1;

if (numG > 0) {
	console.log("Hello");	
}else if (numH > 0) {
	console.log("World");
}

// else staement
/*
1. Executes a statement if all other conditions are false
2. The "else" statement is optioan and cam ne added to capture any other resul to change the flow of the program.
*/

if (numG > 0){
	console.log("Hello");
}else if (numH == 0){
	console.log("World");
}else{
	console.log("Again");
}


// if, else if and else statement with functions

let message= "No Message";
console.log(message);

function determinTyphoonIntensity(windSpeed) {
	if(windSpeed < 30){
		return "Not a typhoon yet.";
	}else if(windSpeed <= 61){
		return "Tropical depression dected";
	}else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical Storm Detected";
	}else if (windSpeed >= 89 && windSpeed <= 117){
		return "Severe Tropical Storm Detected";
	}else{
		return "Typhoon Detected";
	}	

}

// Return the string to the variable "message" that invoked it
message = determinTyphoonIntensity(70);
console.log(message);

if(message == "Tropical Storm Detected"){
		console.warn(message);
	}

message = determinTyphoonIntensity(112);
console.log(message);

message = determinTyphoonIntensity(120);
console.log(message);

// [Section] - Truthy and Falsy
/* 
- In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
- Values are considered true unless defined otherwise
- Falsy values/exceptions for truthy:
1. false
2. 0
3. -0
4. ""
5. null
6. undefined
7. NaN
*/

if(1){
	console.log("truthy");
}

if([]){
	console.log("truthy");
}

// falsy Examples
if(false){
	console.log("falsy");
}
if(0){
	console.log("falsy");
}
if(undefined){
	console.log("falsy");
}

// [SECTION] - Conditional (Ternary) Operator
/* 
- The Conditional (Ternary) Operator takes in three operands:
1. condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is falsy
- Can be used as an alternative to an "if else" statement
- Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
- Commonly used for single statement execution where the result consists of only one line of code
- For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
- Syntax
(expression) ? ifTrue : ifFalse;
*/

// Single statement execution

let	ternaryResult1 = (1 < 18) ? true : false;
console.log("Result of ternary operator: " + ternaryResult1);

let	ternaryResult2 = (100 < 18) ? true : false;
console.log("Result of ternary operator: " + ternaryResult2);

// Multiple statement execution
// Both function perform two seperate task which changes the value of the "name" variable and returns the result storing it in the "legalAge" variable

let name;

function isOfLegaAge() {
	name = "John";
	return "Your are of the legal age limit";
}

function isUnderAge() {
	name = "Jane";
	return "Your are of the legal age limit";
}

let age = parseInt(prompt("What is your age?"));
//console.log(typeof age);
console.log(age);

let legaAge = (age > 18) ? isOfLegaAge() : isUnderAge();

console.log("Result of ternary operator in function: \n" +  legaAge + ", " + name);

// [SECTION] - Switch Statement

/* 
- Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input
- The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
- The "expression" is the information used to match the "value" provided in the switch cases
- Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
- Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
- The "break" statement is used to terminate the current loop once a match has been found
- Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a
a match was found
- Syntax
switch (expression) {
case value:
statement;
break;
default:
statement;
break;
}
*/
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);


switch (day){
	case 'monday':
		console.log("The color of the day is red");
		break;
	case 'tuesday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thursday':
		console.log("The color of the day is gree");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'saturday':
		console.log("The color of the day is indigo");
		break;
	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please input a valid day");
		break;
}

// [SECTION] - Try-Catch-Finally Statement

function showIntesirtAlert(windSpeed) {
	try{
		alertat(determinTyphoonIntensity(windSpeed));
	}catch(error){
		console.log(typeof error);
		console.warn(error.message);

	}finally{
		alert("Internsity updates will show new alert");
	}
}

showIntesirtAlert(56);